# DataURI-QuickAction

This is a macOS “Quick Action” that will convert any image to a DataURI.

Requires macOS 10.14 “Mojave” or (presumably) newer.
